from django.contrib import admin

# Register your models here.

from .models import Usuario
from .models import Estadio
from .models import Actividad
from .models import Matricula

admin.site.register(Usuario)
admin.site.register(Estadio)
admin.site.register(Actividad)
admin.site.register(Matricula)

