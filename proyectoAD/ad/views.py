from django.shortcuts import render
from django.shortcuts import render_to_response
from django.http import HttpResponse
from django.views.generic import ListView
from django.core.exceptions import ObjectDoesNotExist

from ad.models import Usuario
from ad.models import Actividad
from ad.models import Matricula

# Create your views here.

#class administrador:
def index(request):
	#return render('index.html')
	return render_to_response('TP_logUsuario.html')
    	#return HttpResponse(output)

def principal(request):
	param_d = request.POST.get("us", "")
	if param_d == '':
		param_nom = request.POST.get("post_name", "")
		param_pass = request.POST.get("post_pass", "")
		try:
			usuario = Usuario.objects.get(nombre=param_nom,password=param_pass)
			contenido = {
				'usuario' : usuario,
		    	}
			return render_to_response('TP_menu.html',contenido)
		except ObjectDoesNotExist, e:
			contenido = {
				'mensaje' : 'El usuario no existe',
		    	}
			return render_to_response('TP_error.html', contenido)
	else:
		usuario = Usuario.objects.get(pk=param_d)
		contenido = {
			'usuario' : usuario,
	    	}
		return render_to_response('TP_menu.html',contenido)
		


def todos(request):
	param_d = request.POST.get("us", "")
	if param_d <> '':
		usuario = Usuario.objects.get(pk=param_d)
		entradas = Actividad.objects.all()
		contenido = {
			'usuario' : usuario,
			'actividades' : entradas,
	    	}
		return render_to_response('TP_todas.html',contenido)


def precio(request):
	param_d = request.POST.get("us", "")
	if param_d <> '':
		param_precio = request.POST.get("precio", "")
		usuario = Usuario.objects.get(pk=param_d)
		entradas = Actividad.objects.all()
		if param_precio <> '':
			if param_precio <> 'ninguno':
				entradas = entradas.filter(precio__lte=param_precio)
		contenido = {
			'usuario' : usuario,
			'actividades' : entradas,
	    	}
		return render_to_response('TP_precio.html',contenido)


def estadio(request):
	param_d = request.POST.get("us", "")
	if param_d <> '':
		param_estadio = request.POST.get("estadioo", "")
		usuario = Usuario.objects.get(pk=param_d)
		entradas = Actividad.objects.all()
		if param_estadio <> '':
			if param_estadio <> 'ninguno':
				entradas = entradas.filter(estadio__nombre__exact=param_estadio)
		contenido = {
			'usuario' : usuario,
			'actividades' : entradas,
	    	}
		return render_to_response('TP_estadio.html',contenido)

def matriculado(request):
	param_d = request.POST.get("us", "")
	if param_d <> '':
		usuario = Usuario.objects.get(pk=param_d)
		entradas = Matricula.objects.all().filter(usuario__exact=param_d)
		contenido = {
			'usuario' : usuario,
			'actividades' : entradas,
	    	}
		return render_to_response('TP_matriculado.html',contenido)

def salirse(request):
	param_d = request.POST.get("us", "")
	if param_d <> '':
		usuario = Usuario.objects.get(pk=param_d)
		entradas = Matricula.objects.all().filter(usuario__exact=param_d)
		contenido = {
			'usuario' : usuario,
			'actividades' : entradas,
	    	}
		return render_to_response('TP_salirse.html',contenido)

def ingreso(request):
	param_d = request.POST.get("us", "")
	if param_d <> '':
		usuario = Usuario.objects.get(pk=param_d)
		entradas = Actividad.objects.all()
		matriculado = Matricula.objects.all().filter(usuario__exact=param_d)
		l = [None] * 100
		for matricula in matriculado:
			entradas = entradas.exclude(pk=matricula.actividad.pk)

		contenido = {
			'usuario' : usuario,
			'actividades' : entradas,
	    	}
		return render_to_response('TP_ingreso.html',contenido)


def Ringreso(request):
	param_d = request.POST.get("us", "")
	param_a = request.POST.get("ac", "")
	if param_d <> '' and param_a <> '':
		if param_a <> 'ninguno':
			usuarion = Usuario.objects.get(pk=param_d)
			actividadn = Actividad.objects.get(pk=param_a)
			matricula = Matricula(usuario=usuarion, actividad=actividadn)
			matricula.save()
			contenido = {
				'usuario' : usuarion,
				'actividad' : actividadn,
		    	}
			return render_to_response('TP_resultado_ingreso.html',contenido)

def Rsalir(request):
	param_d = request.POST.get("us", "")
	param_m = request.POST.get("mat", "")
	if param_d <> '' and param_m <> '':
		if param_a <> 'ninguno':
			usuarion = Usuario.objects.get(pk=param_d)
			matricula = Matricula.objects.get(pk=param_m)
			actividadn = Actividad.objects.get(pk=matricula.actividad.pk)
			matricula.delete()
			contenido = {
				'usuario' : usuarion,
				'actividad' : actividadn,
		    	}
			return render_to_response('TP_resultado_salir.html',contenido)


