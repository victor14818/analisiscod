"""proyectoAD URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from ad import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^log/', views.index, name='index'),
    url(r'^principal/', views.principal, name='principal'),
    url(r'^todos/', views.todos, name='todos'),
    url(r'^precio/', views.precio, name='precio'),
    url(r'^estadio/', views.estadio, name='estadio'),
    url(r'^matriculado/', views.matriculado, name='matriculado'),
    url(r'^salirse/', views.salirse, name='salirse'),
    url(r'^ingreso/', views.ingreso, name='ingreso'),
    url(r'^resultado_ingreso/', views.Ringreso, name='Ringreso'),
    url(r'^resultado_salir/', views.Rsalir, name='Rsalir'),
]
